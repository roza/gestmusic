# Django Class Based Views

Little demo of Django Generic Views

## Install

### Create virtualenv and activate it

```bash
virtualenv -p python3 venv
source venv/bin/activate
```

### Install dependencies

```bash
pip install -r requirements
```

### Create and populate DataBase

```bash
./manage.py migrate
./manage.py loaddata initial_musiques_data.json 
```

### Launch server and test

```bash
./manage.py runserver
```

And visit : http://localhost:8000/musiques !

## or ... use gitpod 


[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/roza/gestmusic)