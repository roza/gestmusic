from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from musiques.models import Artiste, Morceau


# Create your tests here.

class MorceauTestCase(TestCase):
    def setUp(self):
        artiste1 = Artiste.objects.create(nom='Paco de Lucia')
        artiste2 = Artiste.objects.create(nom='Pink Floyd')
        Morceau.objects.create(titre='music1', artiste=artiste1)
        Morceau.objects.create(titre='music2', artiste=artiste2)
        Morceau.objects.create(titre='music3', artiste=artiste2)

    def test_morceau_url_name(self):
        try:
            url = reverse('musiques:morceau_detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_morceau_url(self):
        morceau = Morceau.objects.get(titre='music1')
        url = reverse('musiques:morceau_detail', args=[morceau.pk])
        response = self.client.get(url)
        assert response.status_code == 200
