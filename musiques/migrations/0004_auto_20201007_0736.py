# Generated by Django 3.1.2 on 2020-10-07 07:36

from django.db import migrations


def migrer_artistes(apps, schema):
    Morceau = apps.get_model('musiques', 'Morceau')
    Artiste = apps.get_model('musiques', 'Artiste')
    artistes_connus = [
        field['artiste'] for field in Morceau.objects.all().values('artiste').distinct()
    ]
    for artiste in artistes_connus:
        Artiste.objects.create(nom='artiste')


def annuler_migrer_artistes(apps, schema):
    Artiste = apps.get_model('musiques', 'Artiste')
    Artiste.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('musiques', '0003_artiste'),
    ]

    operations = [
        migrations.RunPython(migrer_artistes,
                             reverse_code=annuler_migrer_artistes)
    ]
