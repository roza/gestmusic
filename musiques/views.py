
from django.shortcuts import render

from .models import Morceau
from django.views.generic import DetailView
from django.views.generic import ListView

# Create your views here.
from django.http import HttpResponse


def morceau_detail(request, pk):
    return HttpResponse('OK')


class MorceauDetailView(DetailView):
    model = Morceau


class MorceauList(ListView):
    model = Morceau
